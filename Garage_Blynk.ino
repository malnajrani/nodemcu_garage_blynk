#define BLYNK_PRINT Serial
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
char auth[] = "AuthCode";
char ssid[] = "WiFiName";
char pass[] = "WiFiPassword";
const int trigPin = 5; //D1 on NODEMCU CP2102
const int echoPin = 4; //D2 on NODEMCU CP2102
long duration = 0;
int distance = 0;
int counter = 0;
String LDR_st;
String dist_st;
BlynkTimer timer;

void sendSensor(){
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration * 0.034 / 2;
  if (distance < 10) {           
    dist_st = "Closed";
    counter = 0;
  }
  
  else if (distance > 9 && distance < 500) {
    dist_st = "Open";
    counter ++; // counting how many secs since dist_st is "open" , in void setup() 1000L=1sec
  }

  else {
      dist_st = "invaild";
      counter = 0;
  }

  // You can send any value at any time.
  // Please don't send more that 10 values per second.
  Blynk.virtualWrite(V0, distance);
  Blynk.virtualWrite(V1, dist_st);

}

void setup(){
  Serial.begin(9600);
  //Blynk.begin(auth, ssid, pass, IPAddress(10, 192, 168, 232), 8080); //ONLY for private server with IP:10.192.168.232 & port:8080
  Blynk.begin(auth, ssid, pass);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  timer.setInterval(1000L, sendSensor);
}

void loop(){
  Blynk.run();
  timer.run();
}
